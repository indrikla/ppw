from django.test import TestCase
from django.test import LiveServerTestCase, TestCase, tag, Client
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.urls import reverse, resolve
from django.http import HttpRequest
from . import views
from django.contrib.auth.models import User

# Create your tests here.
class Story9UnitTest(TestCase):
    def test_if_url_exsit(self):
        response = Client().get("/login")
        self.assertEquals(response.status_code, 200)

    def test_template_used(self):
        response = Client().get("/login")
        self.assertTemplateUsed(response, "login.html")

    def test_view_template_logged_in(self):
        response = Client().get("/hi")
        self.assertTemplateUsed(response, "hi.html")
    
    def test_form_validation_accepted(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'secret'}
        User.objects.create_user(**self.credentials)
        form = AuthenticationForm(data=self.credentials)
        self.assertTrue(form.is_valid())

    def test_register_url_exist_view_template_used(self):
        response = Client().get("/signup")
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "registration.html")