from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('hi', views.hi, name='hi'),
    path('login', views.loginFunc, name='login'),
    path('logout', views.logoutFunc, name='logout'),
    path('signup', views.signup, name='signup')
]