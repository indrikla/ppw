from django.shortcuts import redirect, render
from django.http import HttpResponse

# Create your views here.
def story7(request):
    return render(request, "story7.html")
