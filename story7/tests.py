from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from django.http import HttpRequest
from . import views

# Create your tests here.
class Story7UnitTest(TestCase):
    def test_url_data_is_exist(self):
        response = Client().get("/story7")
        self.assertEquals(301, response.status_code)

    def test_template_form(self):
        response = Client().get("/story7/")
        self.assertTemplateUsed(response, "story7.html")

