from django.forms import ModelForm
from .models import Jadwal


class JadwalForm(ModelForm):
    required_css_class = 'required'

    class Meta:
        model = Jadwal
        fields = '__all__'
