from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('tambah/', views.jadwal, name= "tambahMatkul"),
    path('data/', views.data, name="dataMatkul"),
    path('data/<int:index>/', views.detail, name="detail"),
]