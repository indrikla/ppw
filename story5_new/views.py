from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Jadwal
from .forms import JadwalForm


# Create your views here.
def jadwal(request):
    form = JadwalForm()
    if request.method == 'POST':
        forminput = JadwalForm(request.POST)
        if forminput.is_valid():
            forminput.save()
            current_data = Jadwal.objects.all()
            return render(request, 'tambah_matkul.html', {'form':form, 'status': 'success', 'data' : current_data})
        else:
            current_data = Jadwal.objects.all()
            return render(request, 'tambah_matkul.html', {'form':form, 'status': 'failed', 'data' : current_data})
            
    else: #Kalo method GET
        current_data = Jadwal.objects.all()
        return render(request, 'tambah_matkul.html', {'form':form, 'data':current_data})


def data(request):
    if request.method == 'POST':
        jadwal = Jadwal.objects.all()
        Jadwal.objects.get(id=request.POST['id']).delete()
        return redirect(data);
    else:
        jadwal = Jadwal.objects.all()
        return render(request, 'data_matkul.html', {'jadwal': jadwal})

def detail(request, index):
        jadwal = Jadwal.objects.get(pk=index)
        return render(request, 'detail.html', {'jadwal': jadwal})