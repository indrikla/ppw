from django.contrib import admin
from . import models

# Register your models here.
@admin.register(models.Jadwal)
class JadwalAdmin(admin.ModelAdmin):
    list_display = ('matkul', 'dosen', 'sks', 'semester', 'ruang', 'deskripsi')
    search_fields = ('matkul', 'dosen', 'sks', 'semester', 'ruang')
