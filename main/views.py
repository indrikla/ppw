from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, "index.html")

def comingsoon(request):
    return render(request, "comingsoon.html")
    
def stories(request):
    return render(request, "stories.html")
