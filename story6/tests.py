from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from django.http import HttpRequest
from .models import Kegiatan, Partisipan
from .forms import KegiatanForm, PartisipanForm
from .views import tambahKegiatan, dataKegiatan

# Create your tests here.
class Story6UnitTest(TestCase):
    def test_activity_url_data_is_exist(self):
        response = Client().get("/kegiatan/data")
        self.assertEquals(301, response.status_code)

    def test_template_form(self):
        response = Client().get("/kegiatan/data/")
        self.assertTemplateUsed(response, "data_kegiatan.html")

    def test_model_create2(self):
        kegiatan1 = Kegiatan.objects.create(kegiatan="yeehaw")
        Partisipan.objects.create(kegiatan = kegiatan1, nama="aku")
        jumlah = Partisipan.objects.all().count()
        self.assertEquals(jumlah, 1)

    def test_new_partisipan_form_validation(self):
        kegiatan_new = Kegiatan.objects.create(kegiatan = "aw")
        form = PartisipanForm(data = {'kegiatan' :kegiatan_new, 'nama' : 'Unit testing'})
        self.assertTrue(form.is_valid())

    def test_data_view_show(self):
        Kegiatan.objects.create(kegiatan='Test')
        request = HttpRequest()
        response = dataKegiatan(request)
        isi_html = response.content.decode('utf8')
        self.assertIn('Test', isi_html)
    
    def test_insert_blank_partisipan(self):
        kegiatan_new = Kegiatan.objects.create(kegiatan = "ya allah ppw")
        form = PartisipanForm(data = {'kegiatan' :kegiatan_new, 'nama' : ''})
        self.assertEqual(form.errors['nama'], ["This field is required."])

    def test_activity_url_tambah_is_exist(self):
        response = Client().get("/kegiatan/tambah/")
        self.assertEquals(200, response.status_code)

    def test_template_form(self):
        response = Client().get("/kegiatan/tambah/")
        self.assertTemplateUsed(response, "tambah_kegiatan.html")

    def test_view_form(self):
        response = Client().get("/kegiatan/tambah/")
        isi_html = response.content.decode("utf8")
        self.assertIn('Tambah <span><br>kegiatan', isi_html)
        self.assertIn('<form action="" method="POST" novalidate>', isi_html)

    def test_model_create(self):
        Kegiatan.objects.create(kegiatan="test")
        jumlah = Kegiatan.objects.all().count()
        self.assertEquals(jumlah, 1)

    def test_model_relational(self):
        Kegiatan.objects.create(kegiatan = "owo")
        kegiatan_id = Kegiatan.objects.all()[0].id
        kegiatan_di_id = Kegiatan.objects.get(id=kegiatan_id)
        Partisipan.objects.create(kegiatan = kegiatan_di_id, nama="aa")
        jumlah = Partisipan.objects.filter(kegiatan=kegiatan_di_id).count()
        self.assertEquals(jumlah, 1)

    def test_form_validation_accepted(self):
        form = KegiatanForm(data={'kegiatan': 'yow'})
        self.assertTrue(form.is_valid())
    
    def test_model_name(self):
        Kegiatan.objects.create(kegiatan='Test')
        kegiatan = Kegiatan.objects.get(kegiatan='Test')
        self.assertEqual(str(kegiatan),'Test')

    def test_model_name2(self):
        Kegiatan.objects.create(kegiatan='naon')
        idpk = Kegiatan.objects.all()[0].id
        kegiatan = Kegiatan.objects.get(id=idpk)
        Partisipan.objects.create(kegiatan=kegiatan, nama='ihiy')
        peserta = Partisipan.objects.get(kegiatan=kegiatan)
        self.assertEqual(str(peserta),'ihiy')

    def test_model_name3(self):
        form = KegiatanForm({'kegiatan' : ' '})
        self.assertFalse(form.is_valid())

    def test_model_name4(self):
        form = PartisipanForm({'kegiatan' : 'berkebun', 'nama' : ' '})
        self.assertFalse(form.is_valid())

    def test_str_model_to_html(self):
        kegiatan = Kegiatan.objects.create(kegiatan ='menjaga awan')
        response = Client().get('/kegiatan/data/')
        isi_html = response.content.decode('utf8')
        self.assertIn(str(kegiatan), isi_html)


    def test_activity_add_using_func(self):
        found = resolve('/kegiatan/tambah/')
        self.assertEqual(found.func, tambahKegiatan)

@tag('functional')
class IndexFunctionalTest(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()