from django.forms import ModelForm
from .models import Kegiatan, Partisipan

class KegiatanForm(ModelForm):
    required_css_class = 'required'

    class Meta :
        model = Kegiatan
        fields = '__all__'   

class PartisipanForm(ModelForm):
    required_css_class = 'required'

    class Meta :
        model = Partisipan
        fields = '__all__'   
