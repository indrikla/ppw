from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('tambah/', views.tambahKegiatan, name='tambahKegiatan'),
    path('data/', views.dataKegiatan, name='dataKegiatan'),
]