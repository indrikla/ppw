from django.shortcuts import redirect, render
from django.http import HttpResponse, JsonResponse
import requests

# Create your views here.
def buku(request):
    return render(request, "buku.html")

def get_buku(request):
    key = request.GET['key']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + key
    response = requests.get(url)
    response_json = response.json()
    return JsonResponse(response_json, safe=False)