from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from django.http import HttpRequest
from . import views

# Create your tests here.
class Story8UnitTest(TestCase):
    def test_url_data_is_exist(self):
        response = Client().get("/buku")
        self.assertEquals(200, response.status_code)

    def test_template_form(self):
        response = Client().get("/buku")
        self.assertTemplateUsed(response, "buku.html")

    def test_view_show_in_html(self):
        response = Client().get("/buku")
        isi_html = response.content.decode("utf8")
        self.assertIn('<table class="table" id="dataTable" width="100%" cellspacing="0">', isi_html)

    def test_url_getBuku_is_exist(self):
        response = Client().get("/getData?key=cc")
        self.assertEquals(200, response.status_code)
