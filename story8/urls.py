from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('buku', views.buku, name='buku'),
    path('getData', views.get_buku),
]